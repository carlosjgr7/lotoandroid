
  import React from 'react'
  import {ImageBackground, View } from 'react-native'
  const image = { uri: "https://reactjs.org/logo-og.png" };

  const BackgroundImage = (props) => {
    const height =window.height
    const width = window.width
    const resizeMode = props.resizeMode || 'cover' // cover
    return (
        <View>
      <ImageBackground
        style={{flex:1, height: height, width: width, resizeMode: 'cover' }}
        opacity={1}
        source={image}
      >
        {props.children}
      </ImageBackground>
      </View>
    )
  }
  
  export default BackgroundImage