import React, { Component } from "react";
import {createStackNavigator} from 'react-navigation-stack'
import {createAppContainer} from 'react-navigation'
import Login from './modules/login/containers/login'
import Registro from './modules/login/containers/registro';
import Lobby from './modules/lobby/containers/lobby';
import Partida from './modules/partida/containers/partida';
const LoginNavigator = createStackNavigator({
  Login:{
    screen: Login,
    navigationOptions:{
      title:"by SoftAndPlay"
    },
  },
  Registro:{
    screen: Registro,
    navigationOptions:{
      title:"Registro!!!"
    },
  },
Lobby:{
    screen: Lobby,
    navigationOptions:{
      title:"Partidas del Dias!!!"
    },
},
Partida:{
  screen: Partida,
  navigationOptions:{
    title:"<Partida en Juego>"
  },
},
},{headerLayoutPresent:'center'})

export default createAppContainer(LoginNavigator)