import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,  
  Image,
  View,
  ImageBackground,
  Alert,
} from 'react-native';

import { Container, Header, Content, Tab, Tabs,Text } from 'native-base';
import HTML from 'react-native-render-html';
import { ScrollView, Dimensions } from 'react-native';

const htmlContent = `
    <h1>No Hay Ganadores</h1>
`;

export default class Juego extends Component {
  constructor(props) {
    super(props);

    this.state = { 
        open: false,
        balota: require('../../../../assets/cards/tapa.png'),
        u1: require('../../../../assets/cards/tapa.png'),
        u2: require('../../../../assets/cards/tapa.png'),
        u3: require('../../../../assets/cards/tapa.png'),
        u4: require('../../../../assets/cards/tapa.png'),
        u5: require('../../../../assets/cards/tapa.png'),
        ganador: htmlContent,       
     };

    this.socket = new WebSocket('wss://multibingos.net/notifications/partida/');
    this.emit = this.emit.bind(this);
  }

  emit() {
    this.setState(prevState => ({ open: !prevState.open }))
    this.socket.send("It worked!")
  }

  render() {
    return (
        <Container>
        <Tabs>
          <Tab heading="Juego">
          <Container style={styles.container}>
            <View style={styles.grande}>
                <Image source={ this.state.balota } style={{width:210, height:320}}/>
            </View>            
            <View>
                <Text style={styles.titulo5}>Ultimas 5</Text>
                <View style={styles.ultimas}>
                    <Image style={styles.img} source={ this.state.u1 } />
                    <Image style={styles.img} source={ this.state.u2 } />
                    <Image style={styles.img} source={ this.state.u3 } />
                    <Image style={styles.img} source={ this.state.u4 } />
                    <Image style={styles.img} source={ this.state.u5 } />
                </View>
            </View>
        </Container>
          </Tab>
          <Tab heading="Tablas">
          <Text>aaaz</Text>
          </Tab>
          <Tab heading="Ganadores">
          <ScrollView style={{ flex: 1 }}>
                <HTML html={this.state.ganador} imagesMaxWidth={Dimensions.get('window').width} />
            </ScrollView>
          </Tab>
        </Tabs>
      </Container>        
    );
  }

  UNSAFE_componentWillMount() {
    this.socket.onopen = () => {
        console.log('open')
    }
    this.socket.onmessage = ({ data }) =>{
        data = JSON.parse(data)
        let ultimas = JSON.parse(data["ultimas5"])

        if(ultimas.length>=1)
            this.setState({u1:CardSelect(ultimas[0]['fields']['balota'])})
        if(ultimas.length>1)
            this.setState({u2:CardSelect(ultimas[1]['fields']['balota'])})
        if(ultimas.length>2)
            this.setState({u3:CardSelect(ultimas[2]['fields']['balota'])})
        if(ultimas.length>3)
            this.setState({u4:CardSelect(ultimas[3]['fields']['balota'])})
        if(ultimas.length>4)
            this.setState({u5:CardSelect(ultimas[4]['fields']['balota'])})

        this.setState({balota: CardSelect(data['balota'])})
        if(data['ganador'] !='none' ){
            setTimeout(function(){Alert.alert('Hay Ganador')}, 1000);
            this.setState({ganador:data['ganador']})
            console.log(data['ganador'])
        }
    }
  }

}
function CardSelect (card){
    let carta= require('../../../../assets/cards/tapa.png')

    if(card==1)
        carta = require('../../../../assets/cards/1.png')                             
    if(card==2)
        carta = require('../../../../assets/cards/2.png')
    if(card==3)
        carta = require('../../../../assets/cards/3.png')
    if(card==4)
        carta = require('../../../../assets/cards/4.png')
    if(card==5)
        carta = require('../../../../assets/cards/5.png')
    if(card==6)
        carta = require('../../../../assets/cards/6.png')
    if(card==7)
        carta = require('../../../../assets/cards/7.png')
    if(card==8)
        carta = require('../../../../assets/cards/8.png')
    if(card==9)
        carta = require('../../../../assets/cards/9.png')
    if(card==10)
        carta = require('../../../../assets/cards/10.png')
    if(card==11)
        carta = require('../../../../assets/cards/11.png')
    if(card==12)
        carta = require('../../../../assets/cards/12.png')
    if(card==13)
        carta = require('../../../../assets/cards/13.png')
    if(card==14)
        carta = require('../../../../assets/cards/14.png')
    if(card==15)
        carta = require('../../../../assets/cards/15.png')            
    if(card==16)
        carta = require('../../../../assets/cards/16.png')            
    if(card==17)
        carta = require('../../../../assets/cards/17.png')            
    if(card==18)
        carta = require('../../../../assets/cards/18.png')            
    if(card==19)
        carta = require('../../../../assets/cards/19.png')            
    if(card==20)
        carta = require('../../../../assets/cards/20.png')            
    if(card==21)
        carta = require('../../../../assets/cards/21.png')                      
    if(card==22)
        carta = require('../../../../assets/cards/22.png')                      
    if(card==23)
        carta = require('../../../../assets/cards/23.png')                      
    if(card==24)
        carta = require('../../../../assets/cards/24.png')                      
    if(card==25)
        carta = require('../../../../assets/cards/25.png')                      
    if(card==26)
        carta = require('../../../../assets/cards/26.png')                      
    if(card==27)
        carta = require('../../../../assets/cards/27.png')                      
    if(card==28)
        carta = require('../../../../assets/cards/28.png')                      
    if(card==29)
        carta = require('../../../../assets/cards/29.png')                      
    if(card==30)
        carta = require('../../../../assets/cards/30.png')                      
    if(card==31)
        carta = require('../../../../assets/cards/31.png')                      
    if(card==32)
        carta = require('../../../../assets/cards/32.png')                      
    if(card==33)
        carta = require('../../../../assets/cards/33.png')                      
    if(card==34)
        carta = require('../../../../assets/cards/34.png')                      
    if(card==35)
        carta = require('../../../../assets/cards/35.png')                      
    if(card==36)
        carta = require('../../../../assets/cards/36.png')                      
    if(card==37)
        carta = require('../../../../assets/cards/37.png')                      
    if(card==38)
        carta = require('../../../../assets/cards/38.png')                      
    if(card==39)
        carta = require('../../../../assets/cards/39.png')                      
    if(card==40)
        carta = require('../../../../assets/cards/40.png')                      
    if(card==41)
        carta = require('../../../../assets/cards/41.png')                      
    if(card==42)
        carta = require('../../../../assets/cards/42.png')                      
    if(card==43)
        carta = require('../../../../assets/cards/43.png')                      
    if(card==44)
        carta = require('../../../../assets/cards/44.png')                      
    if(card==45)
        carta = require('../../../../assets/cards/45.png')                      
    if(card==46)
        carta = require('../../../../assets/cards/46.png')                      
    if(card==47)
        carta = require('../../../../assets/cards/47.png')                      
    if(card==48)
        carta = require('../../../../assets/cards/48.png')                      
    if(card==49)
        carta = require('../../../../assets/cards/49.png')                      
    if(card==50)
        carta = require('../../../../assets/cards/50.png')                      
    if(card==51)
        carta = require('../../../../assets/cards/51.png')                      
    if(card==52)
        carta = require('../../../../assets/cards/52.png')                      
    if(card==53)
        carta = require('../../../../assets/cards/53.png')                      
    if(card==54)
        carta = require('../../../../assets/cards/54.png') 

    return carta

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
  },
  grande: {
    padding: 20,
  },
  ultimas: {
      flexDirection:"row",
  },
  img:{
    width:80,
    height:120,  
    marginLeft:3

  },
  titulo5:{
      fontSize:20,
      textAlign:"center",
      padding:5,
      fontWeight:"bold"
  },
  image:{
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"  
  }
});

AppRegistry.registerComponent('Juego', () => Juego);