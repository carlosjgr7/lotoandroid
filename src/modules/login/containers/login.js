import React, { Component, useState } from "react";
import { Container,  Content, Card, CardItem, Text, Body, Button, Item, Input, Icon  ,TextInput} from "native-base";
import {StyleSheet, Image, View, Alert  } from 'react-native'


class Login extends Component {
  constructor(props){
    super(props)
    this.state = {
      token: 'undefinedaaaaa',
      username: 'administrador',
      password: 'asd12345',
    }

  }

  registro  = () =>{
    this.props.navigation.navigate('Registro')
  }

  loguear = async () => {

    const data = {username:this.state.username,password:this.state.password};
    fetch('https://multibingos.net/api-v01/generate-token/', {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    body:JSON.stringify(data),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      
      if (typeof responseJson.token === 'undefined'){
          Alert.alert('Usuario o Clave incorreectos')
        }else{
          console.log('response object:',responseJson.token)
          this.setState({token:responseJson.token})
          this.props.navigation.navigate('Lobby',{token:this.state.token})

        }
        
    })
    .catch((error) => {
      console.error(error);
    });

  }
  render() {

    return (
      <Container  style={styles.fondo}>
          <View style={styles.logo}>
            <Image  source={require('../../../../assets/logo.png')} style={styles.img} />     
          </View>
          <Content padder>
          <Card>
              <CardItem header bordered>
                <Text>Login</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <Item inlineLabel>
                    <Icon active name="person"/>
                    <Input  ref="username" value ={this.state.username} placeholder="Usuario" onChangeText={(text) => this.setState({username:text})}/>
                  </Item>
                  <Item inlineLabel last>
                  <Icon active name="key"/>
                    <Input placeholder="Contraseña" ref="password" value ={this.state.password} onChangeText={(text) => this.setState({password:text})} />
                  </Item>
                </Body>
              </CardItem>
              <CardItem footer bordered >
              <Button success onPress={this.loguear}>
                  <Text>Entrar</Text>
                </Button>                  
                <Button style={{marginLeft:5}} onPress={this.registro}>
                  <Text>Registrate</Text>
                </Button>  
              </CardItem>
            </Card>

          </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  fondo : {

    backgroundColor:"#ccc"
    
  },
  logo:{
    flex:.3,
    alignItems:"center",
    marginTop:20
  },
})

export default Login