import React, { Component } from "react";
import { Container,  Content, Card, CardItem, Text, Body, Button, Item, Input, Icon  } from "native-base";
import {StyleSheet, Image, View } from 'react-native'


export default class Registro extends Component {
  render() {
    return (
      <Container  style={styles.fondo}>
          <View style={styles.logo}>
            <Image  source={require('../../../../assets/logo.png')} style={styles.img} />     
          </View>
          <Content padder>
          <Card>
              <CardItem header bordered>
                <Text>Registrate</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <Item inlineLabel>
                    <Icon active name="person"/>
                    <Input placeholder="Usuario" name="user"/>
                  </Item>
                  <Item inlineLabel last>
                  <Icon active name="key"/>
                    <Input placeholder="Contraseña" name="pass" />
                  </Item>
                </Body>
              </CardItem>
              <CardItem footer bordered >
              <Button>
                  <Text>Entrar</Text>
                </Button>                  
                <Button style={{marginLeft:5}}>
                  <Text>Registrate</Text>
                </Button>  
              </CardItem>
            </Card>

          </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  fondo : {

    backgroundColor:"#ccc"
    
  },
  logo:{
    flex:.3,
    alignItems:"center",
    marginTop:20
  },
})