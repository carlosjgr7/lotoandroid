import React, {Component} from 'react';
import {  StyleSheet,  View, FlatList, Alert } from 'react-native';
import { Card, CardItem, Text, Body, Thumbnail, Button , Icon} from "native-base";
import { withNavigation } from 'react-navigation';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },

});
class Partida extends Component {
  constructor(props){
    super(props)
    this.state = {
      partidas:[]
    }

  }
  
 card = (props) =>{

  return(
    <Card>
    <CardItem bordered>
    <Thumbnail small  source={require('../../../../assets/logo.png')} style={styles.img} />
    <Text>{props.descripcion}{"\n"}Partida:{props.id}</Text>
    </CardItem>
    <CardItem footer>
    <Button disabled={props.inicio} ><Icon  active name="cart"/></Button>
    <Button 
          disabled={!props.inicio} 
          style={{marginLeft:10}}
          onPress={()=>this.props.navigation.navigate('Partida',{id:props.id.index_id})}>
          <Icon  active name="keypad"/></Button>
    </CardItem>
    </Card>
  )
    }

  render(){
  
    return (

    <View style={styles.container}>
      <FlatList
        keyExtractor = { (item, index) => index.toString() }
        data={this.props.partidas}
        renderItem={({item}) => this.card(item)}
      />
    </View>
  );
}
}
export default withNavigation(Partida);



