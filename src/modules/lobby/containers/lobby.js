import React, { Component, useState, useEffect } from "react";
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Thumbnail,
} from "native-base";
import {
  StyleSheet,
  ActivityIndicator,
  Image,
  View,
  Alert,
} from "react-native";
import Partida from "../components/partida";

class Lobby extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.navigation.state.params.token,
      partidas: [],
      loading: false,
    };
  }

  UNSAFE_componentWillMount() {
    //const data = {fecha:'2020-08-30'};
  
    fetch("https://multibingos.net/api-v01/partidas/", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "token " + this.state.token,
      },
      //data:JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("response object:", responseJson);
        this.setState({ partidas: responseJson.results, loading: false });
      })
      .catch((error) => {
        console.error(error);
      
      });
  }

  render() {
    const p = this.state.partidas;
    return (
      <Container style={styles.fondo}>
        <View style={styles.logo}>
          <Thumbnail
            large
            source={require("../../../../assets/logo.png")}
            style={styles.img}
          />
          <Text style={{ fontSize: 25 }}>Multibingos.net</Text>
        </View>
        <View style={{ flex: 1 }}>
          {
          this.state.loading?
          <ActivityIndicator size="large" color="#00ff00" />:
          <Partida partidas={p} />
          }
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  fondo: {
    backgroundColor: "#ccc",
  },
  logo: {
    flex: 0.2,

    flexDirection: "row",
    alignItems: "center",
    marginTop: 5,
  },
});

export default Lobby;
